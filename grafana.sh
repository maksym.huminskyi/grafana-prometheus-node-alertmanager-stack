#!/bin/bash

# Install grafana

sudo yum install grafana

# Enabling grafana

sudo systemctl enable grafana-server
sudo systemctl start grafana-server
