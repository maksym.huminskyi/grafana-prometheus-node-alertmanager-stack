#!/bin/bash

# Download node_exporter and unzip it
# For the script to run correctly, uncomment the VERSION variable. 
# The current value is the current latest version. 
# In case it changes, please update the variable accordingly

#VERSION=0.16.0

wget https://github.com/prometheus/node_exporter/releases/download/v${VERSION}/node_exporter-${VERSION}.linux-amd64.tar.gz
tar xvzf node_exporter-${VERSION}.linux-amd64.tar.gz

# Run node_exporter binary
./node_exporter

# Enabling node exporter

sudo systemctl enable node_exporter
sudo systemctl start node_exporter

