
#!/bin/bash

# Download prometheus and unzip it

# For the script to run correctly, uncomment the VERSION variable.
# The current value is the current latest version.
# In case it changes, please update the variable accordingly

#VERSION=2.32.1
wget https://github.com/prometheus/prometheus/releases/download/v${VERSION}/prometheus-${VERSION}.linux-amd64.tar.gz

tar xvzf prometheus-${VERSION}.linux-amd64.tar.gz

#Creating a user for Prometheus 

sudo useradd --no-create-home --shell /bin/false prometheus

# Move the configuration files necessary from the downloaded repository to /etc/prometheus
# Uncomment this section abd add the correct file (the current location of the downloaded configuration files)
#mv /path/where/conf_files/were/downloaded/prometheus.yml /etc/prometheus
#mv /path/where/conf_files/were/downloaded/rules.yml /etc/prometheus

# Changing ownership of prometheus and its configuration files

sudo chown prometheus:prometheus /etc/prometheus/prometheus.yml
sudo chown prometheus:prometheus /etc/prometheus/rules.yml

# Enabling prometheus

sudo systemctl enable prometheus 
sudo systemctl start prometheus
