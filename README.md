# 1. Copy the repository :

    git clone 

# 2. Install Prometheus :

    (sudo) ./prometheus.sh

Please note that you should open the script and uncomment the Version variable and add the path of the configuration files location for the script to run. You can open it in vi/vim/nano or any other text editor.

# 3. Install Node_exporter:

    sudo ./node_exporter.sh

Please note that you should open the script and uncomment the Version variable and add the path of the configuration files location for the script to run. You can open it in vi/vim/nano or any other text editor.

# 4. Install Grafana: 

    (sudo) ./grafana.sh

# 5. Install Alertmanager: 

    (sudo) ./alertmanager.sh

# NB!
The configuration files and rules for Prometheus and Alertmanager are located in the conf_files folder in this repository. However, you need to move them to the correct folder after downloading (/etc/prometheus).
