#!/bin/bash

# Install alertmanager

sudo yum install prometheus-alertmanager

# Move the configuration files necessary from the downloaded repository to /etc/prometheus
# Uncomment this section abd add the correct file location (the current location of the downloaded configuration files)
#mv /path/where/conf_files/were/downloaded/alertmanager.yml /etc/prometheus

# Change ownership of the file

sudo chown -R prometheus:prometheus /etc/prometheus/alertmanager.yml

# Reloading daemon and enabling grafana

sudo systemctl enable grafana-server
sudo systemctl start grafana-server


